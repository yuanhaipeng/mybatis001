package com.qf.test3;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

/**
 * 2019/11/8
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Demo2 {
    public static void main(String[] args) {
        //测试一级缓存sqlSession，保证使用的是同一个sqlSessionFactory
        try {
            Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            SqlSessionFactory f = builder.build(reader);

            SqlSession session1 = f.openSession();
            List<Object> list = session1.selectList("com.qf.dao.StudentDao.findall");
            session1.close();
            System.out.println("==========================");
            SqlSession session2=f.openSession();
            List<Object> list2 = session2.selectList("com.qf.dao.StudentDao.findall");
            session2.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
