package com.qf.test;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.bean.Student;
import com.qf.dao.impl.StudentDaoImpl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 2019/11/5
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Demo1 {
    //事务
    public static void main(String[] args) {
        StudentDaoImpl studentDao = new StudentDaoImpl();
        //1.指定分页参数  startPage(页码值,每页显示条数)
        PageHelper.startPage(4,5);
        //2.调取查询方法
        List<Student> findall = studentDao.findall();
        System.out.println("findall-count="+findall.size());
        //3.将查询结果封装到PageInfo工具类中
        PageInfo pageInfo = new PageInfo(findall);
        //4.从pageInfo中获得分页数据
        List<Student> list = pageInfo.getList();
        System.out.println("list-count="+list.size());
        for (Student student : list) {
            System.out.println(student);
        }
        System.out.println("当前页码:"+pageInfo.getPageNum());
        System.out.println("每页显示条数:"+pageInfo.getPageSize());
        System.out.println("上一页:"+pageInfo.getPrePage());
        System.out.println("下一页:"+pageInfo.getNextPage());
        System.out.println("总条数:"+pageInfo.getTotal());
        System.out.println("总页数:"+pageInfo.getPages());
      /* Student student = new Student(0, "彭帅1", "男", "seletall");
        int i = studentDao.insertStudent(student);
        System.out.println(i);
        System.out.println(student.getSid());*/

     /*   Student student1 = studentDao.findbyid(35);
        System.out.println(student1);*/
      /*  List<Student> students = studentDao.findbyname("张三", "女");
        for (Student student : students) {
            System.out.println(student);
        }*/
       /* Map map = studentDao.find2();
        Set keys = map.keySet();
        Iterator iterator = keys.iterator();
        while(iterator.hasNext()){
            String key =(String) iterator.next();
            System.out.println(key+","+map.get(key));
        }*/
    }
}
