package com.qf.test;

import com.qf.bean.Student;
import com.qf.dao.impl.StudentDaoImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * 2019/11/5
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Demo2 {
    //事务
    public static void main(String[] args) {
        StudentDaoImpl studentDao = new StudentDaoImpl();
       // List<Student> students = studentDao.findbyids(new int[]{111, 30, 4, 5, 6});

       /* ArrayList arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add(3);*/
        //List<Student> students = studentDao.findbyidslist(arrayList);
      //  List<Student> students = studentDao.findbyids2(arrayList,"王五");
        //List<Student> students = studentDao.findbylike("王", "男", -1);
        List<Student> students = studentDao.findbetween("2019-11-2", "2019-11-3");
        for (Student student : students) {
            System.out.println(student);
        }
    }
}
