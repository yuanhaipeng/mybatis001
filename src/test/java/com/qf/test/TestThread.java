package com.qf.test;

import java.util.ArrayList;
import java.util.List;

/**
 * 2019/11/6
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class TestThread {
    private List<String> strlist=new ArrayList<String>();
   // private String str;
    private ThreadLocal<String> threadLocal=new ThreadLocal<String>();
    class A extends  Thread{
        @Override
        public void run() {
            System.out.println("A线程开始....");
            strlist.add("strlist");
          //  str="str的值";
            threadLocal.set("str的值");
            System.out.println("A-Threadlocal="+threadLocal.get());
        }
    }
    class B extends  Thread{
        @Override
        public void run() {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("B线程开始");
            System.out.println("list="+strlist.get(0));
         //   System.out.println("str="+str);
            System.out.println("B-Threadlocal="+threadLocal.get());
        }
    }
    public static void main(String[] args) {
        TestThread testThread = new TestThread();
        TestThread.A a=testThread.new A();
        TestThread.B b=testThread.new B();
        a.start();
        b.start();
    }
}
