package com.qf.test2;

import com.qf.bean.Grade;
import com.qf.bean.Husband;
import com.qf.bean.Student;
import com.qf.bean.Wife;
import com.qf.dao.WifeDao;
import com.qf.dao.impl.GradeDaoImpl;
import com.qf.dao.impl.WifeDaoImpl;

import java.util.List;

/**
 * 2019/11/8
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Demo2 {

    public static void main(String[] args) {

        WifeDaoImpl wifeDao = new WifeDaoImpl();
        List<Husband> husbands = wifeDao.findhus();
        for (Husband husband : husbands) {
            System.out.println(husband);
        }
        System.out.println("----------------------------------");
        List<Wife> wives = wifeDao.findwife();
        for (Wife wife : wives) {
            System.out.println(wife);
        }
    }
}
