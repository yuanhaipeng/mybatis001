package com.qf.test2;

import com.qf.bean.Grade;
import com.qf.bean.Student;
import com.qf.bean.Subject;
import com.qf.dao.impl.GradeDaoImpl;

import java.util.List;

/**
 * 2019/11/8
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Demo1 {

    public static void main(String[] args) {
        GradeDaoImpl gradeDao = new GradeDaoImpl();
     /*   List<Student> students = gradeDao.findAllStudent();
        for (Student student : students) {
            System.out.println(student.getSname1()+","+student.getGrade().getCname());
            List<Subject> subjectList = student.getGrade().getSubjectList();
            for (Subject subject : subjectList) {
                System.out.println(subject.getSubjectName());
            }
            System.out.println("--------------------------------");
        }*/
       List<Grade> grade = gradeDao.findAllGrade();
        for (Grade grade1 : grade) {
            System.out.println(grade1.getCname());
            List<Student> studentList = grade1.getStudentList();
            for (Student student : studentList) {
                System.out.println("\t"+student);
            }
            List<Subject> subjectList = grade1.getSubjectList();
            for (Subject subject : subjectList) {
                System.out.println("\t"+subject.getSubjectName());
            }
        }
    }
}
