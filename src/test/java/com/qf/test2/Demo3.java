package com.qf.test2;

import com.qf.bean.Husband;
import com.qf.bean.Menu;
import com.qf.bean.Role;
import com.qf.bean.Wife;
import com.qf.dao.impl.MenuDaoImpl;
import com.qf.dao.impl.WifeDaoImpl;

import java.util.List;

/**
 * 2019/11/8
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Demo3 {

    public static void main(String[] args) {

        MenuDaoImpl menuDao = new MenuDaoImpl();
        List<Menu> findmenus = menuDao.findmenus();
        for (Menu menu : findmenus) {
            System.out.println(menu.getMenuName());
            List<Role> roleList = menu.getRoleList();
            for (Role role : roleList) {
                System.out.println("\t"+role.getRname());
            }
        }
    }
}
