package com.qf.test4;

import com.qf.bean.Student;
import com.qf.dao.StudentDao2;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.util.List;

/**
 * 2019/11/9
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Demo2 {
    public static void main(String[] args) {
        try {
            //省略dao层的实现类
            SqlSession session =
                    new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml")).openSession();
            //加载接口
            StudentDao2 studentDao2 = session.getMapper(StudentDao2.class);
            List<Student> students = studentDao2.findstudent2();
            for (Student student : students) {
                System.out.println(student);
            }
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
