package com.qf.test4;

import com.qf.bean.Grade;
import com.qf.bean.Student;
import com.qf.dao.impl.GradeDaoImpl;

import java.util.List;

/**
 * 2019/11/9
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Demo1 {

    public static void main(String[] args) {
        GradeDaoImpl gradeDao = new GradeDaoImpl();
//        List<Grade> grades = gradeDao.findgrades();
//        for (Grade grade : grades) {
//            System.out.println(grade.getCname());
//            List<Student> studentList = grade.getStudentList();
//            for (Student student : studentList) {
//                System.out.println("\t"+student);
//            }
//        }
        List<Student> studentList = gradeDao.findAllStudent();
        for (Student student : studentList) {
            System.out.println(student);
        }
    }
}
