package com.qf.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 2019/11/5
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
//多方
public class Student implements Serializable {
    private  Integer sid;
    private String sname1;
    private String sex;
    private String password;

    private Grade grade;

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    private Date birthday;

    @Override
    public String toString() {
        return "Student{" +
                "sid=" + sid +
                ", sname1='" + sname1 + '\'' +
                ", sex='" + sex + '\'' +
                ", password='" + password + '\'' +
                ", grade=" + grade +
                ", birthday=" + birthday +
                '}';
    }

    public Student() {
    }

    public Student(Integer sid, String sname1, String sex, String password) {
        this.sid = sid;
        this.sname1 = sname1;
        this.sex = sex;
        this.password = password;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getSname1() {
        return sname1;
    }

    public void setSname1(String sname1) {
        this.sname1 = sname1;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
