package com.qf.bean;

/**
 * 2019/11/8
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Wife {
    private Integer wifeId;
    private String wifeName;
    private Husband husband;

    @Override
    public String toString() {
        return "Wife{" +
                "wifeId=" + wifeId +
                ", wifeName='" + wifeName + '\'' +
                ", husband=" + husband +
                '}';
    }

    public Integer getWifeId() {
        return wifeId;
    }

    public void setWifeId(Integer wifeId) {
        this.wifeId = wifeId;
    }

    public String getWifeName() {
        return wifeName;
    }

    public void setWifeName(String wifeName) {
        this.wifeName = wifeName;
    }

    public Husband getHusband() {
        return husband;
    }

    public void setHusband(Husband husband) {
        this.husband = husband;
    }
}
