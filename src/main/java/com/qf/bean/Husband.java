package com.qf.bean;

/**
 * 2019/11/8
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class Husband {
    private Integer hid;
    private String  husname;
    private Wife wife;

    @Override
    public String toString() {
        return "Husband{" +
                "hid=" + hid +
                ", husname='" + husname + '\'' +
                ", wife=" + wife +
                '}';
    }

    public Integer getHid() {
        return hid;
    }

    public void setHid(Integer hid) {
        this.hid = hid;
    }

    public String getHusname() {
        return husname;
    }

    public void setHusname(String husname) {
        this.husname = husname;
    }

    public Wife getWife() {
        return wife;
    }

    public void setWife(Wife wife) {
        this.wife = wife;
    }
}
