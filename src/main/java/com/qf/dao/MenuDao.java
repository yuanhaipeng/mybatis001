package com.qf.dao;

import com.qf.bean.Menu;
import com.qf.bean.Role;

import java.util.List;

public interface MenuDao {
    public List<Role> findroles();
    public List<Menu> findmenus();
}
