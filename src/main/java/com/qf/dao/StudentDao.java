package com.qf.dao;

import com.qf.bean.Student;

import java.util.List;
import java.util.Map;

public interface StudentDao {
    //全查
    public List<Student> findall();
    //新增
    public int insertStudent(Student student);
    //修改
    public int updateStudent(Student student);
    //删除
    public int deleteStudent(int id);
    //主键查询
    public Student findbyid(int id);
    //带条件查询
    public  List<Student> findbyname(String name,String sex);
    //聚合函数查询
    public Map   find2();
    //查询特定用户的列表
    public  List<Student> findbyids(int[] ids);
    public List<Student> findbyidslist(List list);
    public List<Student> findbyids2(List idlist,String name);

    //模糊查询的方法
    public List<Student> findbylike(String name,String sex,int id);
    //区间查询
    public List<Student> findbetween(String begintime,String endtime);
}
