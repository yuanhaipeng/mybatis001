package com.qf.dao;

import com.qf.bean.Grade;
import com.qf.bean.Student;

import java.util.List;

public interface GradeDao {
    //查询所有的学生信息
   public List<Student> findAllStudent();
    //查询所有的年级信息
    public List<Grade> findAllGrade();
    //查询年级信息(通过单表查询)
    public List<Grade> findgrades();
}
