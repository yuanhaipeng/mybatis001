package com.qf.dao;

import com.qf.bean.Husband;
import com.qf.bean.Wife;

import java.util.List;

public interface WifeDao {
    //查询妻子信息
    public List<Wife> findwife();
    //查询丈夫信息
    public List<Husband> findhus();
}
