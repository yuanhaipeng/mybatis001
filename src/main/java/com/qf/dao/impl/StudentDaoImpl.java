package com.qf.dao.impl;

import com.qf.bean.Student;
import com.qf.dao.StudentDao;
import com.qf.util.SessionUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 2019/11/5
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class StudentDaoImpl extends SessionUtils implements StudentDao {
    public List<Student> findall() {

        List<Student> list = null;
        SqlSession session=null;
        try {
          /*  Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            SqlSessionFactory sessionFactory = builder.build(reader);
            session = sessionFactory.openSession();*/
          session = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"))
                    .openSession();
            //sqlSession用来执行sql语句
            //selectList("namespace+id")
          // list = session.selectList("com.qf.dao.StudentDao.findall",null,new RowBounds(5,5));
            list = session.selectList("com.qf.dao.StudentDao.findall");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            session.close();
        }

        return list;
    }

    public int insertStudent(Student student) {
        int k=0;
        SqlSession session=null;
        try {
            session = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"))
                    .openSession();
            //sqlSession用来执行sql语句
            //selectList("namespace+id")
            k= session.insert("com.qf.dao.StudentDao.insertStudent", student);
            session.commit();
        } catch (IOException e) {
            e.printStackTrace();
            session.rollback();;
        }finally {
            session.close();
        }
        return k;
    }

    public int updateStudent(Student student) {
        int k=0;
        SqlSession session=null;
        try {
            session = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"))
                    .openSession();
            //sqlSession用来执行sql语句
            //selectList("namespace+id")
            k= session.update("com.qf.dao.StudentDao.updateStudent", student);
            session.commit();
        } catch (IOException e) {
            e.printStackTrace();
            session.rollback();;
        }finally {
            session.close();
        }
        return k;
    }

    public int deleteStudent(int id) {
        int k=0;
        SqlSession session=null;
        try {
            session = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"))
                    .openSession();
            //sqlSession用来执行sql语句
            //selectList("namespace+id")
            k= session.delete("com.qf.dao.StudentDao.deleteStudent", id);
            session.commit();
        } catch (IOException e) {
            e.printStackTrace();
            session.rollback();;
        }finally {
            session.close();
        }
        return k;
    }

    public Student findbyid(int id) {
        SqlSession session=null;
        Student student=null;
        try {
            session = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"))
                    .openSession();
            //sqlSession用来执行sql语句
            //selectList("namespace+id")
           student = session.selectOne("com.qf.dao.StudentDao.findbyid", id);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            session.close();
        }
        return student;
    }


    public List<Student> findbyname(String name, String sex) {
        List<Student> objects = null;
        try {
            SqlSession session=
                    new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml")).openSession();

            Map map=new HashMap();
            map.put("uname",name);
            map.put("usex",sex);
            objects = session.selectList("com.qf.dao.StudentDao.findbyname", map);

            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return objects;
    }

    public Map find2() {
        Map map = null;

        try {
            SqlSession session = getsession();
            map = (Map) session.selectOne("com.qf.dao.StudentDao.find2");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            closesession();
        }

        return map;
    }

    public List<Student> findbyids(int[] ids) {
        SqlSession sqlSession = getsession();
        List<Student> list = sqlSession.selectList("com.qf.dao.StudentDao.findbyids", ids);
        closesession();
        return list;
    }

    public List<Student> findbyidslist(List list) {
        SqlSession sqlSession = getsession();
        List<Student> studentlist = sqlSession.selectList("com.qf.dao.StudentDao.findbyidslist", list);
        closesession();
        return studentlist;
    }

    public List<Student> findbyids2(List idlist, String name) {
        SqlSession sqlSession = getsession();
        Map map=new HashMap();
        map.put("ids",idlist);
        map.put("uname",name);
        List<Student> studentlist = sqlSession.selectList("com.qf.dao.StudentDao.findbyids2", map);
        closesession();
        return studentlist;
    }

    public List<Student> findbylike(String name, String sex, int id) {
        SqlSession sqlSession = getsession();
        Map map=new HashMap();
        map.put("uname",name);
        map.put("usex",sex);
        map.put("uid",id);
        List<Student> list = sqlSession.selectList("com.qf.dao.StudentDao.findbylike", map);
        closesession();
        return list;
    }

    public List<Student> findbetween(String begintime, String endtime) {
        SqlSession sqlSession = getsession();
        Map map=new HashMap();
        map.put("begin",begintime);
        map.put("end",endtime);
        List<Student> list = sqlSession.selectList("com.qf.dao.StudentDao.findbetween", map);
        closesession();
        return list;
    }
}
