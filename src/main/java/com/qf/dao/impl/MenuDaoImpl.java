package com.qf.dao.impl;

import com.qf.bean.Menu;
import com.qf.bean.Role;
import com.qf.dao.MenuDao;
import com.qf.util.SessionUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * 2019/11/8
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class MenuDaoImpl extends SessionUtils implements MenuDao {
    public List<Role> findroles() {
        SqlSession session=getsession();
        //selectList(接口路径+方法名)
        List<Role> objects = session.selectList("com.qf.dao.MenuDao.findroles");
        closesession();
        return objects;
    }

    public List<Menu> findmenus() {
        SqlSession session=getsession();
        //selectList(接口路径+方法名)
        List<Menu> objects = session.selectList("com.qf.dao.MenuDao.findmenus");
        closesession();
        return objects;
    }
}
