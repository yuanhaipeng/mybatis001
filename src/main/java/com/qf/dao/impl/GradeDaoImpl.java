package com.qf.dao.impl;

import com.qf.bean.Grade;
import com.qf.bean.Student;
import com.qf.dao.GradeDao;
import com.qf.util.SessionUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * 2019/11/8
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class GradeDaoImpl extends SessionUtils implements GradeDao {
    public List<Student> findAllStudent() {
        SqlSession sqlSession = getsession();
        List<Student> objects = sqlSession.selectList("com.qf.dao.GradeDao.findAllStudent");
        closesession();
        return objects;
    }

    public List<Grade> findAllGrade() {
        SqlSession sqlSession = getsession();
        List<Grade> objects = sqlSession.selectList("com.qf.dao.GradeDao.findAllGrade");
        closesession();
        return objects;
    }

    public List<Grade> findgrades() {
        SqlSession sqlSession = getsession();
        List<Grade> objects = sqlSession.selectList("com.qf.dao.GradeDao.findgrades");
        closesession();
        return objects;
    }
}
