package com.qf.dao.impl;

import com.qf.bean.Grade;
import com.qf.bean.Husband;
import com.qf.bean.Student;
import com.qf.bean.Wife;
import com.qf.dao.GradeDao;
import com.qf.dao.WifeDao;
import com.qf.util.SessionUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * 2019/11/8
 * Administrator
 * mybatis001
 * 面向对象面向君  不负代码不负卿
 */
public class WifeDaoImpl extends SessionUtils implements WifeDao {
    public List<Wife> findwife() {
        SqlSession sqlSession = getsession();
        List<Wife> objects = sqlSession.selectList("com.qf.dao.WifeDao.findwife");
        closesession();
        return objects;
    }

    public List<Husband> findhus() {
        SqlSession sqlSession = getsession();
        List<Husband> objects = sqlSession.selectList("com.qf.dao.WifeDao.findhus");
        closesession();
        return objects;
    }


}
